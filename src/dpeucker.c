/* -*- coding: utf-8 -*- */
/**
\ingroup geom
@{
\file dpeucker.c
\brief Definición de funciones para el aligerado de polilíneas, basadas en el
       algoritmo de Douglas-Peucker.
\author José Luis García Pallero, jgpallero@gmail.com
\note Este fichero contiene funciones paralelizadas con OpenMP.
\date 17 de agosto de 2013
\copyright
Copyright (c) 2013-2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"libgeoc/dpeucker.h"
/******************************************************************************/
/******************************************************************************/
size_t* AligeraPolilinea(const double* x,
                         const double* y,
                         const size_t nPtos,
                         const size_t incX,
                         const size_t incY,
                         const double tol,
                         const int paralelizaTol,
                         const enum GEOC_DPEUCKER_ROBUSTO robusto,
                         const size_t nSegRobOrig,
                         const size_t nSegRobAuto,
                         const int esf,
                         size_t* nPtosSal)
{
    //identificador de caso especial
    int hayCasoEspecial=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //vector de salida
    size_t* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos casos especiales
    sal = CasosEspecialesAligeraPolilinea(x,y,nPtos,incX,incY,atol,nPtosSal,
                                          &hayCasoEspecial);
    //comprobamos si ha habido algún caso especial
    if(hayCasoEspecial)
    {
        //comprobamos si ha ocurrido algún error de asignación de memoria
        if(nPtos&&(sal==NULL))
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        else
        {
            //salimos de la función
            return sal;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos con el algoritmo original o con el robusto
    if(robusto==GeocDPeuckerOriginal)
    {
        //versión original del algoritmo
        sal = DouglasPeuckerOriginal(x,y,nPtos,incX,incY,atol,esf,nPtosSal);
    }
    else
    {
        //utilizamos la variación robusta del algoritmo
        sal = DouglasPeuckerRobusto(x,y,nPtos,incX,incY,atol,paralelizaTol,
                                    robusto,nSegRobOrig,nSegRobAuto,esf,
                                    nPtosSal);
    }
    //comprobamos los posibles errores
    if(nPtos&&(sal==NULL))
    {
        //mensaje de error
        GEOC_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
size_t* DouglasPeuckerOriginal(const double* x,
                               const double* y,
                               const size_t nPtos,
                               const size_t incX,
                               const size_t incY,
                               const double tol,
                               const int esf,
                               size_t* nPtosSal)
{
    //índice para recorrer bucles
    size_t i=0;
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //variables auxiliares
    int aux=0;
    size_t pos=0;
    //vector de identificadores de elementos usados
    char* usados=NULL;
    //vector de salida
    size_t* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //iniciamos la variable de salida de número de puntos a 0
    *nPtosSal = 0;
    //asigno memoria para los puntos usados y la inicializo a ceros (calloc)
    usados = (char*)calloc(nPtos,sizeof(char));
    //comprobamos los posibles errores
    if(usados==NULL)
    {
        //mensaje de error
        GEOC_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    //indico el primer punto y el último como usados
    usados[0] = 1;
    usados[nPtos-1] = 1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos sobre la esfera o sobre el plano
    if(esf)
    {
        //aplico el algoritmo original sobre la esfera
        DouglasPeuckerOriginalEsfera(x,y,nPtos,incX,incY,atol,0,nPtos-1,usados);
    }
    else
    {
        //aplico el algoritmo original sobre el plano
        DouglasPeuckerOriginalPlano(x,y,nPtos,incX,incY,atol,0,nPtos-1,usados);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cuento los puntos usados
    for(i=0;i<nPtos;i++)
    {
        //copio el identificador de uso en una variable de tipo entero, ya
        //que si no el compilador gcc 4.7.0 da warning si asigno el valor de
        //tipo char a la variable size_t
        //da warning de posible error de conversión por cambio de signo
        //aunque se haga la conversión explícita
        aux = (int)usados[i];
        //voy contando
        (*nPtosSal) += (size_t)aux;
    }
    //asigno memoria para el vector de salida
    sal = (size_t*)malloc((*nPtosSal)*sizeof(size_t));
    //comprobamos los posibles errores
    if(sal==NULL)
    {
        //libero la memoria utilizada
        free(usados);
        //mensaje de error
        GEOC_ERROR("Error de asignación de memoria");
        //salimos de la función
        return NULL;
    }
    //inicio la variable de posición
    pos = 0;
    //recorro el vector de elementos usados
    for(i=0;i<nPtos;i++)
    {
        //compruebo si el elemento se ha usado
        if(usados[i])
        {
            //asigno la posición
            sal[pos] = i;
            //aumento el contador de posiciones en el vector de salida
            pos++;
        }
    }
    //compruebo si sólo se han usado dos puntos que son el mismo
    if((*nPtosSal==2)&&(x[0]==x[(nPtos-1)*incX])&&(y[0]==y[(nPtos-1)*incY]))
    {
        //actualizamos el número de puntos de salida
        *nPtosSal = 1;
        //reasigno memoria
        sal = (size_t*)realloc(sal,(*nPtosSal)*sizeof(size_t));
        //comprobamos los posibles errores
        if(sal==NULL)
        {
            //libero la memoria utilizada
            free(usados);
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //libero la memoria utilizada
    free(usados);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
size_t* DouglasPeuckerRobusto(const double* x,
                              const double* y,
                              const size_t nPtos,
                              const size_t incX,
                              const size_t incY,
                              const double tol,
                              const int paralelizaTol,
                              const enum GEOC_DPEUCKER_ROBUSTO robusto,
                              const size_t nSegRobOrig,
                              const size_t nSegRobAuto,
                              const int esf,
                              size_t* nPtosSal)
{
    //valor absoluto de la tolerancia
    double atol=fabs(tol);
    //identificador de caso especial
    int hayCasoEspecial=0;
    //vector de salida
    size_t* sal=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos casos especiales
    sal = CasosEspecialesAligeraPolilinea(x,y,nPtos,incX,incY,atol,nPtosSal,
                                          &hayCasoEspecial);
    //comprobamos si ha habido algún caso especial
    if(hayCasoEspecial)
    {
        //comprobamos si ha ocurrido algún error de asignación de memoria
        if(nPtos&&(sal==NULL))
        {
            //mensaje de error
            GEOC_ERROR("Error de asignación de memoria");
            //salimos de la función
            return NULL;
        }
        else
        {
            //salimos de la función
            return sal;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si trabajamos sobre la esfera o sobre el plano
    if(esf)
    {
        //aplico el algoritmo robusto sobre la esfera
        sal = DouglasPeuckerRobustoEsfera(x,y,nPtos,incX,incY,atol,
                                          paralelizaTol,robusto,nSegRobOrig,
                                          nSegRobAuto,nPtosSal);
    }
    else
    {
        //aplico el algoritmo robusto sobre el plano
        sal = DouglasPeuckerRobustoPlano(x,y,nPtos,incX,incY,tol,paralelizaTol,
                                         robusto,nSegRobOrig,nSegRobAuto,
                                         nPtosSal);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return sal;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
